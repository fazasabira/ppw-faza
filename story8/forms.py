from django import forms




class SubsmeForm(forms.Form):
    email_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Example: hira@gmail.com',
    }
    usr_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Full usr',
    }
    pass_attrs = {
        'type': 'pwd',
        'class': 'form-control',
        'placeholder':'pwd',
    }
    usr = forms.CharField(max_length = 50, error_messages={"required": "Please enter your usr"}, widget = forms.TextInput(attrs=usr_attrs))
    pwd = forms.CharField(max_length = 20, min_length=6, widget = forms.pwdInput(attrs=pass_attrs), error_messages={"min_length": "pwd must be longer than 5 characters"})
    email = forms.EmailField(error_messages={"required": "Please enter a valid email"}, max_length=50, widget=forms.TextInput(attrs=email_attrs))
