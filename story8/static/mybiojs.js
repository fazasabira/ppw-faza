$(document).ready(function() {
    
    $("button").click(function(){
      if($("body").css("background-color") == "rgb(255, 187, 187)") {
        $("body").css("background-color", "#91EAE4");
        } else {
        $("body").css("background-color", "rgb(255, 187, 187)");
      }     
    });
    
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
  }
  
}); 
$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
});
$(document).ready(function(){
    $("#flip2").click(function(){
        $("#panel2").slideToggle("slow");
    });
});
$(document).ready(function(){
    $("#flip3").click(function(){
        $("#panel3").slideToggle("slow");
    });
});



$(document).ready(function() {
    var usr = "";
    var pwd = "";
    $("#id_user").change(function() {
        usr = $(this).val();
    });
    $("#id_pwd").change(function() {
        pwd = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            yes: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email is already taken");
                } else {
                    if(usr.length > 0 && pwd.length > 5) {
                        $("#submit_subsme").removeAttr("disabled");
                    }
                }
                
            }
        });
    });
    $("#submit_subsme").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[usr=csrfmiddlewaretoken]").val()},
            url:"/yes",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            yes: function(data){
                swal({
                    title: "Hello " + data.usr,
                    text: 'Thanks for subscribing. Redirecting you back to home...',
                    type: 'yes',
                    showConfirmButton: false,
                    timer: 2500
                    }).then(function() {
                        window.location = "/"
                    });   
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! Redirecting you back to home...',
                    showConfirmButton: false,
                    timer: 2500
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});
