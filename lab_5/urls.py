from django.urls import path
from .views import schedule_new, schedule_list, schedule_delete

urlpatterns = [
    path('newsf/', schedule_new),
    path('schedule_list/', schedule_list, name='schedule_list'),
    path('deleteAllSchedule', schedule_delete, name='schedule_delete'),
]