from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Schedule
# Create your views here.

def schedule_delete(request):
    schedule = Schedule.objects.all().delete()
    return render(request, 'ListSchedule.html', {'schedules': schedule})

def schedule_list(request):
    schedule = Schedule.objects.all().values()
    return render(request, 'ListSchedule.html', {'schedules': schedule})

def schedule_new(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            new_sch = form.save()
            new_sch.save()
            return redirect('/lab_5/schedule_list/')
    else:
        form = ScheduleForm()
    return render(request,'Scheduleforms.html', {'form': form})
