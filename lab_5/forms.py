from django import forms

from .models import Schedule

# class Date(forms.DateTimeInput):
#     input_type = 'datetime-local'

class ScheduleForm(forms.ModelForm):

    class Meta:
        model = Schedule
        fields = ('date', 'place', 'event','category')