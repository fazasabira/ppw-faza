from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'lab_4/My Profile.html')

def jastip(request):
    return render(request, 'lab_4/jastip.html')

def jastiphto(request):
    return render(request, 'lab_4/jastiphto.html')

def jastippict(request):
    return render(request, 'lab_4/jastippict.html')

def jastipabout(request):
    return render(request, 'lab_4/jastipabout.html')

def formmember(request):
    return render(request, 'lab_4/formmember.html')
    

def subscribe(request):
    response = {}
    response["forms"] = SubscribeForm()
    return render(request, 'subscribe.html', response)


@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

def success(request):
    submited_form = SubscribeForm(request.POST or None)
    if (submited_form.is_valid()):
        cd = submited_form.cleaned_data
        new_subscriber = Subscriber(name=cd['name'], password=cd['password'], email=cd['email'])
        new_subscriber.save()
        data = {'name': cd['name'], 'password': cd['password'], 'email': cd['email']}
    return JsonResponse(data)
    

