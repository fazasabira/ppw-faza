from django.db import models
from django.core.exceptions import ValidationError


class User(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=32)
    
    @classmethod
    def is_email_registered(cls, value):
        if not cls.objects.filter(email=value).exists():
            raise ValidationError("This e-mail is not registered", params={"value": value})
