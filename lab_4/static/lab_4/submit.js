$(document).ready(function() {
    var usr = "";
    var pwd = "";
    $("#id_user").change(function() {
        usr = $(this).val();
    });
    $("#id_pwd").change(function() {
        pwd = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email is already taken");
                } else {
                    if(usr.length > 0 && pwd.length > 5) {
                        $("#submit_subscribe").removeAttr("disabled");
                    }
                }
                
            }
        });
    });
    $("#submit_subscribe").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[usr=csrfmiddlewaretoken]").val()},
            url:"/success",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "Hello " + data.usr,
                    text: 'Thanks for subscribing. Redirecting you back to home...',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    }).then(function() {
                        window.location = "/"
                    });   
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! Redirecting you back to home...',
                    showConfirmButton: false,
                    timer: 2500
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});
